package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.ProjectCompleteByIndexResponse;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final ProjectCompleteByIndexResponse response = getProjectEndpoint().
                completeProjectStatusByIndex(request);
        @Nullable final Project project = response.getProject();
        if(project==null)throw new ProjectNotFoundException();
        showProject(project);
    }

}
