package ru.t1consulting.nkolesnik.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskGetByIndexResponse;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public final class TaskGetByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        @Nullable Task task = response.getTask();
        if(task==null) throw new TaskNotFoundException();
        showTask(task);
    }

}
