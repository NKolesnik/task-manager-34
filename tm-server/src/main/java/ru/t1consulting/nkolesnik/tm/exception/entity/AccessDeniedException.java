package ru.t1consulting.nkolesnik.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

public final class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
