package ru.t1consulting.nkolesnik.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectGetByIndexRequest(@Nullable String token) {
        super(token);
    }
}
