package ru.t1consulting.nkolesnik.tm.dto.response.task;

import lombok.NoArgsConstructor;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractResponse {

}
