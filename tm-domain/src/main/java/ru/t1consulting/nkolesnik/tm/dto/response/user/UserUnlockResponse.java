package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {

    @Override
    public void setMessage(@NotNull String message) {
        if(super.getSuccess())
            super.setMessage("User successful unlocked");
    }

}

